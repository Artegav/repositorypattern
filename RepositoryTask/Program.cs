﻿using RepositoryTask.Models;
using RepositoryTask.Repository;

namespace RepositoryTask;

public static class Program
{
    public static void Main(string[] args)
    {
        using (ProductContext context = new ProductContext())
        {
            BaseRepository repository = new BookRepository(context);
            Product product = new Product
            {
                Name = "Book",
                Price = 9.99M
            };
            repository.Add(product);
            repository.Save();

            Console.WriteLine($"Product {product.Id} {product.Name} - {product.Price} added.");
        }
        
        Console.WriteLine("Hello, World!");
        Console.ReadKey();
    }
}