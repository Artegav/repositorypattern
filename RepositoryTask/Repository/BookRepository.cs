using RepositoryTask.Models;

namespace RepositoryTask.Repository;

public class BookRepository : BaseRepository
{
    private readonly ProductContext _context;

    public BookRepository(ProductContext context)
    {
        _context = context;
    }
    public override Product GetById(int id)
    {
        return _context.Products.FirstOrDefault(p => p.Id == id) ?? 
               throw new ArgumentException("Product not found", nameof(id));
    }

    public override void Add(Product product)
    { 
        _context.Products.Add(product);
    }

    public override void Update(Product product)
    {
        _context.Products.Update(product);
    }

    public override void Delete(int id)
    {
        _context.Products.Remove(GetById(id));
    }
    
    public override void Save()
    {
        _context.SaveChanges();
    }
}