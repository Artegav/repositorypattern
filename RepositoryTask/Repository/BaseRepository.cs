using RepositoryTask.Models;

namespace RepositoryTask.Repository;

public abstract class BaseRepository
{
    public abstract Product GetById(int id);
    public abstract void Add(Product product);
    public abstract void Update(Product product);
    public abstract void Delete(int id);
    public abstract void Save();
}