using Microsoft.EntityFrameworkCore;
using RepositoryTask.Models;

namespace RepositoryTask;

public class ProductContext : DbContext
{
    public DbSet<Product> Products { get; set; }
}